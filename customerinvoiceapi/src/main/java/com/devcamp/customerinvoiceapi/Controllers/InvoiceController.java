package com.devcamp.customerinvoiceapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.models.Invoice;
import com.devcamp.customerinvoiceapi.services.InvoiceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;
    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoiceApi(){
        return invoiceService.getAllInvoices();
    }
    
    @GetMapping("/invoices/{index}")
    public Invoice getIndexInvoice(@PathVariable int index){
        return invoiceService.getIndexInvoice(index);
    }

}
