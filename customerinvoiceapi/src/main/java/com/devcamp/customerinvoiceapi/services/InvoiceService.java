package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService extends CustomerService {
    @Autowired
    public CustomerService customerService;
    Invoice invoice1 = new Invoice(01, customer1, 200000.0);
    Invoice invoice2 = new Invoice(02, customer1, 300000.0);
    Invoice invoice3 = new Invoice(03, customer2, 300000.0);
    Invoice invoice4 = new Invoice(04, customer3, 400000.0);
    Invoice invoice5 = new Invoice(05, customer4, 100000.0);
    Invoice invoice6 = new Invoice(06, customer5, 250000.0);

    public ArrayList<Invoice> getAllInvoices(){
        ArrayList<Invoice> allInvoiceList = new ArrayList<>();
        allInvoiceList.add(invoice1);
        allInvoiceList.add(invoice2);
        allInvoiceList.add(invoice3);
        allInvoiceList.add(invoice4);
        allInvoiceList.add(invoice5);
        allInvoiceList.add(invoice6);
        return allInvoiceList;
    }

    public Invoice getIndexInvoice(int index){
        Invoice invoice = null;
        ArrayList<Invoice> allInvoiceList = getAllInvoices();
        if (index >= 0 && index <= allInvoiceList.size())
        invoice = allInvoiceList.get(index);
        return invoice;

        }
    

}
